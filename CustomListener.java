package Automation.TestAutomation;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomListener implements ITestListener {

    private static List<ITestResult> failedTests = new ArrayList<>();
    
    public static List<ITestResult> getFailedTests() {
        return failedTests;
    }
    @Override
    public void onTestFailure(ITestResult result) {
        if (isArithmeticException(result.getThrowable())) {
            failedTests.add(result);
        }
    }

    private boolean isArithmeticException(Throwable throwable) {
        return throwable instanceof ArithmeticException;
    }

    @Override
    public void onFinish(ITestContext context) {
        if (!failedTests.isEmpty()) {
            System.out.println("Failed tests:");
            for (ITestResult result : failedTests) {
                System.out.println(result.getTestClass().getName() + " - " + result.getMethod().getMethodName());
            }
        }
    }

    @Override
    public void onTestStart(ITestResult result) {}

    @Override
    public void onTestSuccess(ITestResult result) {}

    @Override
    public void onTestSkipped(ITestResult result) {}

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {}

    @Override
    public void onStart(ITestContext context) {}
}
